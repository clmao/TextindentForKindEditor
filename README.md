KindEditor的一个首行缩进插件
没选中任何文本的情况下，会对所有的P段落进行缩进2em。
文本在选中文本情况下，会对其所在的P段落进行缩进2em或去掉缩进。
使用方法
1.将paragraphindent文件夹放入kindeditor/plugins文件夹下面
2.在初始化编辑器的时候，追加上paragraphindent参数,参考如下：
var editor;
KindEditor.lang({
	paragraphindent: '首行缩进'
});
KindEditor.ready(function(K) {
		editor = K.create('textarea[name="test"]', {
				items : ['source','paragraphindent']

		});

});

3.在kindeditor/themes/default/default.css末尾追加代码

.ke-icon-paragraphindent {
    background-position: 0px -240px;
    width: 16px;
    height: 16px;
}
4.后端开发人员，写得菜不要喷我啊 :joy: 
